<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Types extends Model
{
  protected $table = 'types';

  public $primaryKey = 'id';

  public function pokemon(){
    return $this->belongsToMany('\App\Pokemon');
  }
}
