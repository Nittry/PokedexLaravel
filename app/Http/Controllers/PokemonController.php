<?php

namespace App\Http\Controllers;
use App\Pokemon;
use App\Types;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;


class PokemonController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */

     public function __construct(){
       $this->middleware('auth', ['except' => ['index', 'show']]);
     }

    public function index()
    {
        $pokemon = Pokemon::Orderby('id', 'asc')->paginate(50);
        return view('pokemon.index')->with('pokemon', $pokemon);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::user()->is_admin == 0){
        return redirect('/pokemon')->with('error', 'Unauthorized Page');
      }

        $select_types1 = Types::pluck('name', 'id');
        $select_types2 = Types::pluck('name', 'id');
        $select_types2 = array_add($select_types2, '0', 'nothing');
        $checkbox_types = Types::all();
        $data = array(
          'select_types1' => $select_types1,
          'select_types2' => $select_types2,
          'checkbox_types' => $checkbox_types,
        );
        return view('pokemon.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required',
          'evolution' => 'required',
          '1sttype' => 'required',
          '2ndtype' => 'required',
          'image' => 'image|required|max:1999'

        ]);

        //Hande file upload
        if($request->hasFile('image')){
          $poke_id = DB::table('pokemons')->max('id')+1;
          //Upload Image
          $path = $request->file('image')->storeAs('public/poke_img', $poke_id . '.png');
        }
        //Create Pokemon
        $pokemon = new Pokemon;
        $pokemon->name = $request->input('name');
        $pokemon->evolution = $request->input('evolution');
        $pokemon->save();
        if ($request->input('2ndtype') == '0') {
          DB::table('pokemons_types')->insert([
            ['id_pokemon' => $poke_id, 'id_types' => $request->input('1sttype')]
          ]);
        } else {
          DB::table('pokemons_types')->insert([
            ['id_pokemon' => $poke_id, 'id_types' => $request->input('1sttype')],
            ['id_pokemon' => $poke_id, 'id_types' => $request->input('2ndtype')]
          ]);
        }
        $checkbox_types = Types::all();
        foreach ($checkbox_types as $checkbox_type) {
          if ($request->has($checkbox_type->name)) {
            DB::table('pokemon_weaknesses')->insert([
              ['id_pokemon' => $poke_id, 'id_weaknesses' => $checkbox_type->id]
            ]);
          }
        }
        return redirect('/pokemon')->with('success','Pokemon Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pokemon = Pokemon::find($id);
        $pokemon_types = DB::select('SELECT types.name FROM types JOIN pokemons_types ON types.id=pokemons_types.id_types WHERE pokemons_types.id_pokemon=?',[$id]);
        $pokemon_weaknesses = DB::select('SELECT types.name FROM types JOIN pokemon_weaknesses ON types.id=pokemon_weaknesses.id_weaknesses WHERE pokemon_weaknesses.id_pokemon=?', [$id]);
        $inCollection = DB::table('user_pokemons')->where('id_user', Auth::user()->id)->pluck('id_pokemon');
        $inCollection = (array) $inCollection;
        $inCollection = array_collapse($inCollection);
        $data = array(
              'pokemon' => $pokemon,
              'pokemon_types' => $pokemon_types,
              'pokemon_weaknesses' => $pokemon_weaknesses,
              'inCollection' => $inCollection
        );
        return view('pokemon.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::user()->is_admin == 0){
        return redirect('/pokemon')->with('error', 'Unauthorized Page');
      }

      $pokemon = Pokemon::find($id);
      $select_types1 = Types::pluck('name', 'id');
      $select_types2 = array_add($select_types1, '0', 'nothing');
      $select_types_selected = DB::table('pokemons_types')->where('id_pokemon',$pokemon->id)->pluck('id_types');
      $checkbox_types = Types::all();
      $checkbox_types_selected = DB::table('pokemon_weaknesses')->where('id_pokemon',$pokemon->id)->pluck('id_weaknesses');
      $checkbox_types_selected =  array_collapse((array) $checkbox_types_selected);

      $data = array(
        'select_types1' => $select_types1,
        'select_types2' => $select_types2,
        'checkbox_types' => $checkbox_types,
        'pokemon' => $pokemon,
        'select_types_selected' => $select_types_selected,
        'checkbox_types_selected' => $checkbox_types_selected
      );
      return view('pokemon.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'name' => 'required',
        'evolution' => 'required',
        '1sttype' => 'required',
        '2ndtype' => 'required',
        'image' => 'image|max:1999'

      ]);


      $pokemon = Pokemon::find($id);
      //Hande file upload
      if($request->hasFile('image')){
        //Upload Image
        $path = $request->file('image')->storeAs('public/poke_img', $pokemon->id . '.png');
      }
      //Update Pokemon
      $pokemon->name = $request->input('name');
      $pokemon->evolution = $request->input('evolution');
      $pokemon->save();
      if ($request->input('2ndtype') == '0') {
        DB::table('pokemons_types')->where('id_pokemon', $pokemon->id)->delete();
        DB::table('pokemons_types')->insert([
          ['id_pokemon' => $pokemon->id, 'id_types' => $request->input('1sttype')]
        ]);
      } else {
        DB::table('pokemons_types')->where('id_pokemon', $pokemon->id)->delete();
        DB::table('pokemons_types')->insert([
          ['id_pokemon' => $pokemon->id, 'id_types' => $request->input('1sttype')],
          ['id_pokemon' => $pokemon->id, 'id_types' => $request->input('2ndtype')]
        ]);
      }
      $checkbox_types = Types::all();
      DB::table('pokemon_weaknesses')->where('id_pokemon', $pokemon->id)->delete();
      foreach ($checkbox_types as $checkbox_type) {
        if ($request->has($checkbox_type->name)) {
          DB::table('pokemon_weaknesses')->insert([
            ['id_pokemon' => $pokemon->id, 'id_weaknesses' => $checkbox_type->id]
          ]);
        }
      }
      return redirect('/pokemon')->with('success','Pokemon Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pokemon = Pokemon::find($id);
        $pokemon->delete();
        Storage::delete('/public/storage/poke_img/'.$pokemon->id . 'png');
        return redirect('/pokemon')->with('success', 'Pokemon Deleted');
    }
    public function colAddDel($id)
    {
      $inCollection = DB::table('user_pokemons')->where('id_user', Auth::user()->id)->pluck('id_pokemon');
      $inCollection = (array) $inCollection;
      $inCollection = array_collapse($inCollection);

      if (in_array($id, $inCollection)) {
        //Remove pokemon to collection
          DB::table('user_pokemons')->where('id_pokemon', $id)->delete();
          return redirect('/home/collection')->with('success','Pokemon removed from your collection');
      } else {
        //Add pokemon to collection
          DB::table('user_pokemons')->insert(['id_user' => Auth::user()->id, 'id_pokemon' => $id]);
          return redirect('/home/collection')->with('success','Pokemon added to your collection');
      }
    }
}
