<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
    protected $table = 'pokemons';

    public $primaryKey = 'id';

    public $timestamps = false;

    public function types(){
      return $this->belongsToMany('\App\Types');
    }
}
