@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="container">
                    @if (Auth::user()->is_admin == 1)
                      <a href="/PokedexLaravel/public/pokemon/create" class="btn btn-primary">Create New Pokemon</a>
                    @else
                      <a href="/PokedexLaravel/public/home/collection" class="btn btn-primary">Your Collection</a>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
