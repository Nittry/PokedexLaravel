@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create New Pokemon</div>
                {!! Form::open(['action' => 'PokemonController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group">
                      {{Form::label('name', 'Name')}}
                      {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'New Pokemon Name'])}}
                    </div>
                    <div class="form-group">
                      {{Form::label('evolution', 'Evolution')}}
                      {{Form::number('evolution', '', ['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('1sttype', '1st Type')}}
                          {{Form::select('1sttype', $select_types1)}}
                    </div>
                    <div class="form-group">
                        {{Form::label('2ndtype', '2nd Type')}}
                          {{Form::select('2ndtype', $select_types2, '0')}}
                    </div>
                    <div class="form-group">
                        {{Form::label('weaknesses', 'Weaknesses')}}
                    </div>
                        @foreach ($checkbox_types as $pokemon_type)
                          <div class="form-group">
                          {{Form::label($pokemon_type->name, $pokemon_type->name)}}
                          {{Form::checkbox($pokemon_type->name, $pokemon_type->id) }}
                          </div>
                        @endforeach
                    <div class="form-group">
                        {{Form::file('image')}}
                    </div>
                    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
