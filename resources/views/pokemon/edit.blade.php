@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Pokemon</div>
                {!! Form::open(['action' => ['PokemonController@update', $pokemon->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group">
                      {{Form::label('name', 'Name')}}
                      {{Form::text('name', $pokemon->name, ['class' => 'form-control', 'placeholder' => 'New Pokemon Name'])}}
                    </div>
                    <div class="form-group">
                      {{Form::label('evolution', 'Evolution')}}
                      {{Form::number('evolution', $pokemon->evolution, ['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('1sttype', '1st Type')}}
                          {{Form::select('1sttype', $select_types1, $select_types_selected[0])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('2ndtype', '2nd Type')}}
                        @if (count($select_types_selected) > 1)
                          {{Form::select('2ndtype', $select_types2, $select_types_selected[1])}}
                        @else
                          {{Form::select('2ndtype', $select_types2, '0')}}
                        @endif

                    </div>
                    <div class="form-group">
                        {{Form::label('weaknesses', 'Weaknesses')}}
                    </div>
                        @foreach ($checkbox_types as $pokemon_type)
                          <div class="form-group">
                          {{Form::label($pokemon_type->name, $pokemon_type->name)}}
                          @if (in_array($pokemon_type->id, $checkbox_types_selected))
                            {{Form::checkbox($pokemon_type->name, $pokemon_type->id, true) }}
                          @else
                            {{Form::checkbox($pokemon_type->name, $pokemon_type->id) }}
                          @endif
                          </div>
                        @endforeach
                    <div class="form-group">
                        {{Form::file('image')}}
                    </div>
                    {{Form::hidden('_method', 'PUT')}}
                    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
