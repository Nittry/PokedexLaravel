@extends('layouts.app')
@section('content')
  <div class="container">
    <img class="justify-content-center" style="width:150px;" src="/PokedexLaravel/public/storage/poke_img/{{$pokemon->id}}.png" alt="pokemon image not found">
    <h1>{{$pokemon->name}}</h1>
    <h2>Evolution: {{$pokemon->evolution}}</h2>
    <h2>Types</h2>
    @if(count($pokemon_types) > 0)
      @foreach($pokemon_types as $pokemon_type)
        <p>{{$pokemon_type->name}}</p>
      @endforeach
    @endif
    <h2>Weaknesses</h2>
    @if(count($pokemon_weaknesses) > 0)
      @foreach($pokemon_weaknesses as $pokemon_weakness)
        <p>{{$pokemon_weakness->name}}</p>
      @endforeach
    @endif

    @if (!Auth::guest())
      @if(Auth::user()->is_admin == 1)
        <a href="/PokedexLaravel/public/pokemon/{{$pokemon->id}}/edit" class="btn btn-primary">Edit Pokemon</a>
        {!!Form::open(['action'=> ['PokemonController@destroy', $pokemon->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
        {!!Form::close()!!}

      @else
        @if (!in_array($pokemon->id, $inCollection))
          {!! Form::open(['action' => ['PokemonController@colAddDel', $pokemon->id], 'method' => 'POST']) !!}
          {{Form::submit('Add pokemon to your collection', ['class' => 'btn btn-success'])}}
          {!!Form::close()!!}
        @else
          {!! Form::open(['action' => ['PokemonController@colAddDel', $pokemon->id], 'method' => 'POST']) !!}
          {{Form::submit('Remove pokemon to your collection', ['class' => 'btn btn-danger'])}}
          {!!Form::close()!!}
        @endif
      @endif
    @endif

  </div>
@endsection
