@extends('layouts.app')
@section('content')
    @if(count($pokemon) > 0)
      <div class="d-flex align-content-center flex-wrap mb-3">
        @foreach($pokemon as $poke)

            <a href="/PokedexLaravel/public/pokemon/{{$poke->id}}"><div class="p-2 bd-highlight">
              <img class="justify-content-center" style="width:150px;" src="/PokedexLaravel/public/storage/poke_img/{{$poke->id}}.png" alt="pokemon image not found">
              <h1>{{$poke->name}}</h1>
            </div></a>
        @endforeach
      </div>
      {{$pokemon->links()}}
    @else
      <p>There are no pokemon in our pokedex yet</p>
    @endif
@endsection
